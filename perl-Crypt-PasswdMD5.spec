Name:          perl-Crypt-PasswdMD5
Version:       1.42
Release:       2
Summary:       Provides interoperable MD5-based crypt() functions
License:       GPL-1.0-or-later or Artistic-1.0
URL:           https://metacpan.org/release/Crypt-PasswdMD5
Source0:       https://cpan.metacpan.org/authors/id/R/RS/RSAVAGE/Crypt-PasswdMD5-%{version}.tgz

BuildArch:     noarch
BuildRequires: perl-interpreter perl-generators perl(ExtUtils::MakeMaker) perl(strict) perl(warnings)
BuildRequires: perl(Digest::MD5) >= 2.53  perl(Exporter) perl(Test::More) >= 1.001002
Requires:      perl(Digest::MD5) >= 2.53

%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Digest::MD5\\)$

%description
This perl package provides MD5-based crypt() functions.

%package       help
Summary:       Documents for perl-Crypt-PasswdMD5
Requires:      man info

%description   help
Documents for Package perl-Crypt-PasswdMD5

%prep
%autosetup -p1 -n Crypt-PasswdMD5-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -exec rm -f {} ';'
chmod -R u+w %{buildroot}/*

%check
%make_build test

%files
%license LICENSE
%{perl_vendorlib}/Crypt

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.42-2
- cleanup spec

* Thu Jul 13 2023 leeffo <liweiganga@uniontech.com> - 1.42-1
- upgrade to version 1.42

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.4.1-1
- Upgrade to version 1.4.1

* Mon Mar 9 2020 Chen Dingxiao <chendingxiao1@huawei.com> - 1.4.0-14
- Package init
